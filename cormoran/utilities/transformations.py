import numpy as np

screenY0 = -25.38342902978723
screenYscale = 9.33042553191497E-5

screenX0 = -57.38106014642857
screenXscale = 1.030035714285734E-4


# todo: Model catamaran dynamics here, obtaining a mapping between Vx Vy, and V w,  according to dimensions, headings, e


def single_integrator_to_unicycle(dxi, poses, boat_type, linear_velocity_gain=1, angular_velocity_limit=np.pi / 2):
    m, n = np.shape(dxi)
    if boat_type == 0:

        a = np.cos(poses[2, :])
        b = np.sin(poses[2, :])

        dxu = np.zeros((2, n))
        dxu[0, :] = linear_velocity_gain * (a * dxi[0, :] + b * dxi[1, :])
        dxu[1, :] = angular_velocity_limit * np.arctan2(-b * dxi[0, :] + a * dxi[1, :], dxu[0, :]) / (np.pi / 2)
    elif boat_type == 1:
        # Xw Yw waypoint
        # X Y current
        # z heaging angle
        # Xdiff = Xw - X ;
        # Ydiff = Yw - Y ;
        # dst = sqrt(Xdiff^2 + Ydiff^2) ;
        # hd = atan2(Ydiff,Xdiff) ;
        hd = np.arctan2(dxi[1, :], dxi[0, :])
        # dhd = hd - z ;
        dhd = hd - poses[2, :]
        # F = 0 ;
        # b = 0 ;
        dxu = np.zeros((2, n))

        # if dhd < -pi
        if dhd < -np.pi:
            #   dhd = 2*pi + hd - z ;
            dhd = 2 * np.pi + hd - poses[2, :]
        # elseif dhd > pi
        elif dhd > np.pi:
            #   dhd = -2*pi + hd - z ;
            dhd = -2 * np.pi + hd - poses[2, :]
        # if (dst <= 2.5)
        #     F = 0 ;
        #     b = 0 ;
        # else
        #     if (dhd > -pi/2) && (dhd < pi/2)
        if abs(dhd) < np.pi / 2:
            #   b = bmax*dhd/(pi/2) ;
            dxu[1, :] = angular_velocity_limit * dhd / (np.pi / 2)
            #   F = Fmax ;
            dxu[0, :] = linear_velocity_gain
        #     elseif ((dhd >= -pi)&&(dhd <= -pi/2)) || ((dhd
        # >= pi/2)&&(dhd <= pi))
        elif np.pi / 2 <= abs(dhd) <= np.pi:
            #   b = bmax*(sign(dhd)) ;
            dxu[1, :] = angular_velocity_limit * np.sign(dhd)
            dxu[0, :] = linear_velocity_gain / 2
            #   if (dst <= 10)
            #       F = (Fmax/3)*(dst/10) ;
            #   else
            #       F = Fmax/2 ;

    return dxu


def distance(a, b):
    return np.sqrt((a[0] - b[0]) ** 2 + (a[1] - b[1]) ** 2)


def is_between(a, c, b, epsilon=0.15):
    da = distance(a, c)
    db = distance(c, b)
    dc = distance(a, b)

    is_bet = False
    for k in range(len(da)):
        is_bet = -epsilon < (da[k] + db[k] - dc) < epsilon
        if is_bet:
            break

    return is_bet


def pixels2gps(screen_x, screen_y):
    world_y = screenY0 + screenYscale * screen_y
    world_x = screenX0 + screenXscale * screen_x
    return world_x, world_y


def gps2pixels(world_x, world_y):
    screen_y = (world_y - screenY0) / screenYscale
    screen_x = (world_x - screenX0) / screenXscale
    return round(screen_x), round(screen_y)
