from abc import ABC, abstractmethod

import numpy as np


class CormoranABC(ABC):

    def __init__(self, number_of_agents=1, map_data=None, initial_poses=np.zeros((3, 1)), lidar=None,
                 boundary=None):
        # User options
        if boundary is None:
            boundary = [0, 0, 1000, 1500]

        self.number_of_agents = number_of_agents
        self.map_data = map_data

        # Saving data
        self.file_path = None
        self.current_file_size = 0

        # Constants
        self.max_linear_velocity = 10
        self.max_angular_velocity = np.pi
        self.time_step = 0.033
        self.asv_size = 4
        self.boundary = boundary

        # Variables
        self.velocities = np.zeros((2, self.number_of_agents))
        self.poses = initial_poses
        self.distance_driven = 0
        self.lidar = lidar

        # Saving variables
        self.saved_poses = []
        self.saved_velocities = []

    def set_velocities(self, velocities):
        ids = np.where(np.abs(velocities[0, :]) > self.max_linear_velocity)
        velocities[0, ids] = self.max_linear_velocity * np.sign(velocities[0, ids])

        ids = np.where(np.abs(velocities[1, :]) > self.max_angular_velocity)
        velocities[1, ids] = self.max_angular_velocity * np.sign(velocities[1, ids])

        self.velocities = velocities

    def get_velocities(self):
        return self.velocities

    def get_lidar(self):
        return self.lidar

    def get_distance(self):
        return self.distance_driven

    @abstractmethod
    def step(self):
        pass

    @abstractmethod
    def get_poses(self):
        pass

    @abstractmethod
    def finalize(self):
        pass
