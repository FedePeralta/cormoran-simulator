from cormoran.cormoran import *
from cormoran.utilities.transformations import *

map_data = img.imread('E:\\LSD\\177\\code\\python\\cormoran\\cormoran\\map\\small_map.png')
map_data = np.flipud(map_data[:, :, 0])
map_data = 1 - map_data
N = 1
c = Cormoran(N, initial_poses=np.array(np.mat('600.0; 400.0; 0.0')),
             map_yaml_name='E:\\LSD\\177\\code\\python\\cormoran\\cormoran\\map\\ypacarai_final.yaml')

goal = 0
# goals = np.array([[[430.], [550.]], [[330.], [1290.]], [[500.], [1000.]], [[800.], [300.]]])
goals = np.array([[[430.], [550.]], [[550.], [550.]], [[430.], [550.]], [[550.], [550.]]])
velocity_gain = 1

while True:
    # Get poses of agents
    x = c.get_poses()

    u = goals[goal]
    v = x[:2]
    if (np.round(np.subtract(u, v)) == np.zeros([2, 1])).all():
        goal = goal + 1
        if goal == 4:
            goal = 0
    # Vx Vy
    dxi = velocity_gain * np.subtract(u, v) / np.linalg.norm(np.subtract(u, v))
    # dxi = velocity_gain * np.array([[0], [1]])
    # V w
    dxu = single_integrator_to_unicycle(dxi, x, 1)
    # dxu = np.array((np.mat('1;0')))
    c.set_velocities(dxu)
    c.step()
c.finalize()
