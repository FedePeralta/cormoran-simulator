# todo: comentar todas las funciones
from time import time

from cormoran.cormoran import Cormoran
from cormoran.path_planners.path_planners import a_star
from cormoran.utilities.beacons_handling import *
from cormoran.utilities.transformations import *

c = Cormoran(1, initial_poses=np.array(np.mat('500.0;  500.0; 0.0')),
             map_yaml_name='E:\\LSD\\177\\code\\python\\cormoran\\cormoran\\map\\ypacarai_final.yaml',
             show_figure=False)
if not c.show_figure and False:
    c.initialize_visualization_map_only()

np.seterr(divide='ignore', invalid='ignore')

updated_yaml_file = 'E:\\LSD\\177\\code\\python\\cormoran\\cormoran\\map\\ypacarai_obstacles.yaml'
c.set_updated_map(updated_yaml_file)

beacons = get_position_beacons('E:\\LSD\\177\\code\\python\\cormoran\\cormoran\\map'
                               '\\normalizedRealBeaconsv3.json', 1407.6, np.zeros([2, 1]))
perm_matrix = [25, 59, 26, 7, 44, 2, 42, 55, 21, 30, 53, 22,
               52, 11, 51, 20, 50, 18, 31, 8, 33, 10, 45, 54, 43, 48, 39,
               4, 46, 37, 38, 1, 32, 3, 19, 49, 14, 58, 9, 41, 5, 24, 36,
               12, 47, 6, 35, 17, 57, 23, 16, 29, 13, 56, 40, 0, 28, 27, 15, 34]
beacons = beacons[perm_matrix]
goal = 0
print('Current Goal: ', goal + 1)
# c.draw_points(beacons)
velocity_gain = 1

x = c.get_poses()
c.step()
t1 = time()
path2follow = a_star(c.map_data, np.round(x[:2]).astype(np.int64), x[2], np.round(beacons[goal]).astype(np.int64))
tf = time() - t1
path_goal = 1
path_limit = np.size(path2follow, 0)
# c.draw_path(path2follow)
re_planning = 0
global_planning_count = 1
pathfollowed = (x[:2]).copy()
pathfollowed = np.expand_dims(pathfollowed, 0)
while True:
    # Get poses of agents
    x = c.get_poses()

    u = path2follow[path_goal, :]
    v = x[:2]
    # print(v)
    # print(u)
    # tt = time()
    vR = np.round(v).astype(np.int64)
    uR = np.round(u).astype(np.int64)

    cLidar = c.get_lidar()
    cMapDat = c.get_map_around(vR)

    diff = np.logical_xor(cLidar, cMapDat)
    if diff.any():
        i, j = np.where(diff)
        c.update_map(i, j, vR)

    i, j = np.where(cLidar == 1)
    if i is not []:
        pt = [vR[0] - 3 + j, vR[1] + 3 - i]
        direction = (uR - vR)
        direction = (np.round(direction / np.max(np.abs(direction)))).astype(np.int64)
        if is_between(vR, pt, u) or c.map_data[vR[1] + direction[1][0], vR[0]] \
                or c.map_data[vR[1], vR[0] + direction[0][0]]:
            # pathfollowed = np.concatenate([pathfollowed, [x[:2]]])
            t1 = time()
            path2follow = a_star(c.map_data, np.round(v).astype(np.int64), x[2],
                                 np.round(beacons[goal]).astype(np.int64))
            tf = tf + time() - t1
            global_planning_count = global_planning_count + 1
            path_limit = np.size(path2follow, 0)
            path_goal = 1
            re_planning = re_planning + 1
            u = path2follow[path_goal, :]
            # c.draw_path(path2follow)
            # print(time()-tt)

    if (np.round(np.subtract(u, v)) == np.zeros([2, 1])).all():
        path_goal = path_goal + 1
        pathfollowed = np.concatenate([pathfollowed, [x[:2]]])
        if path_goal == path_limit:
            goal = goal + 1
            if goal == 60:
                break
            print('Goal Reached.\nTimes replanned:', re_planning,
                  '\nCurrent Goal: ', goal + 1)
            re_planning = 0
            # # c.draw_path(pathfollowed)
            t1 = time()
            path2follow = a_star(c.map_data, np.round(x[:2]).astype(np.int64), x[2],
                                 np.round(beacons[goal]).astype(np.int64))
            tf = tf + time() - t1
            global_planning_count = global_planning_count + 1
            path_goal = 1
            path_limit = np.size(path2follow, 0)
            # c.draw_path(path2follow)
    dxi = velocity_gain * np.subtract(u, v) / np.linalg.norm(np.subtract(u, v))
    dxu = single_integrator_to_unicycle(dxi, x, 0)
    c.set_velocities(dxu)
    c.step()
print('Crash count:', c.crash_count)
print('Objective Completed')
tf = tf / global_planning_count
print('Avg. Time:', tf)
print('in: ', global_planning_count, ' planned paths.')
c.finalize()
