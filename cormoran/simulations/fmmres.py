from cormoran.cormoran import *
from cormoran.path_planners.path_planners import fmm, update_fmm_around

map_data = img.imread('E:\\LSD\\177\\code\\python\\cormoran\\cormoran\\map\\small_map.png')
map_data = np.flipud(map_data[:, :, 0])
map_data = 1 - map_data
new_map_data = img.imread('E:\\LSD\\177\\code\\python\\cormoran\\cormoran\\map\\small_map_new.png')
new_map_data = np.flipud(new_map_data[:, :, 0])
new_map_data = 1 - new_map_data

point_around = np.array([[57], [100]])

print(point_around)
print(np.array([[50], [90]]))
path2follow, t, ms, v = fmm(map_data, np.array([[50], [90]]), np.array([[78], [137]]),
                            fmm_type=1)
print('a')

path2followu, tu, nu, v = update_fmm_around(map_data, np.array([[50], [90]]), np.array([[78], [137]]), point_around, 15,
                                            t.copy(), ms.copy(), v.copy())
print('a')

path2follown, tn, ns, v = fmm(new_map_data, np.array([[50], [90]]), np.array([[78], [137]]),
                                     fmm_type=1)
print('a')

plt.subplot(231)
plt.imshow(t, origin='lower')
plt.plot(path2follow[:, 0], path2follow[:, 1], color='red', marker='o', markersize=2)
plt.title('Main FMS, Original Map.')

plt.subplot(234)
plt.imshow(ms, origin='lower')
plt.plot(path2follow[:, 0], path2follow[:, 1], color='red', marker='o', markersize=2)
plt.title('Obstacle FMM, Original Map.')

plt.subplot(233)
plt.imshow(tu, origin='lower')
plt.plot(path2followu[:, 0], path2followu[:, 1], color='red', marker='o', markersize=2)
plt.title('Main FMS, Updated method.')

plt.subplot(236)
plt.imshow(nu, origin='lower')
plt.plot(path2followu[:, 0], path2followu[:, 1], color='red', marker='o', markersize=2)
plt.title('Obstacle FMM, Updated method.')

plt.subplot(232)
plt.imshow(tn, origin='lower')
plt.plot(path2follown[:, 0], path2follown[:, 1], color='red', marker='o', markersize=2)
plt.title('Main FMS, Normal method.')

plt.subplot(235)
plt.imshow(ns, origin='lower')
plt.plot(path2follown[:, 0], path2follown[:, 1], color='red', marker='o', markersize=2)
plt.title('Obstacle FMM, Normal method.')

plt.show()

plt.subplot(131)
plt.plot(path2follow[:, 0], path2follow[:, 1], color='red', marker='o', markersize=2)
plt.axis('equal')
plt.title('Main FMS, Original Map.')

plt.subplot(133)
plt.plot(path2followu[:, 0], path2followu[:, 1], color='red', marker='o', markersize=2)
plt.axis('equal')
plt.title('Main FMS, Updated method.')

plt.subplot(132)
plt.plot(path2follown[:, 0], path2follown[:, 1], color='red', marker='o', markersize=2)
plt.axis('equal')
plt.title('Main FMS, Normal method.')

plt.show()
