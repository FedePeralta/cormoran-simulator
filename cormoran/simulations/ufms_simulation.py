from time import time

from cormoran.cormoran import Cormoran
from cormoran.path_planners.path_planners import update_fmm_around
from cormoran.utilities.beacons_handling import *
from cormoran.utilities.transformations import *

c = Cormoran(1, initial_poses=np.array(np.mat('500.0;  500.0; 0.0')),
             map_yaml_name='E:\\LSD\\177\\code\\python\\cormoran\\cormoran\\map\\ypacarai_final.yaml',
             show_figure=True)
if not c.show_figure and False:
    c.initialize_visualization_map_only()
beacons = get_position_beacons('E:\\LSD\\177\\code\\python\\cormoran\\cormoran\\map'
                               '\\normalizedRealBeaconsv3.json', 1407.6, np.zeros([2, 1]))
perm_matrix = [25, 59, 26, 7, 44, 2, 42, 55, 21, 30, 53, 22,
               52, 11, 51, 20, 50, 18, 31, 8, 33, 10, 45, 54, 43, 48, 39,
               4, 46, 37, 38, 1, 32, 3, 19, 49, 14, 58, 9, 41, 5, 24, 36,
               12, 47, 6, 35, 17, 57, 23, 16, 29, 13, 56, 40, 0, 28, 27, 15, 34]
beacons = beacons[perm_matrix]

goal = 0
print('Current Goal: ', goal + 1)
c.draw_points(beacons)
velocity_gain = 1

x = c.get_poses()
c.step()
to = time()

dist_driven = []
cum_dist = 0

v = np.round(x[:2]).astype(np.int64)

base_dir = "E:\\Fiuna\\Tesis\\Software\\super fms fast calculation 3000"

name = '\\FMS' + str(goal) + '_vel.txt'
toa = np.loadtxt(base_dir + name, delimiter=',')
name = '\\FMS' + str(goal) + '_toa.txt'
vel = np.loadtxt(base_dir + name, delimiter=',')

path2follow = update_fmm_around(t=toa, ms=vel, v=np.full(c.map_data.shape, 110),
                                map_data=c.map_data, point_around=v, radius=15,
                                goal_pose=np.round(beacons[goal]).astype(np.int64), current_pose=v)
# path2follow = gradient_descent(toa, v, np.round(beacons[goal]).astype(np.int64))
# print(path2follow)
tf = time() - to
path_goal = 1
path_limit = np.size(path2follow, 0)
c.draw_path(path2follow, markersize=1)
while True:
    x = c.get_poses()

    u = path2follow[path_goal, :]
    v = x[:2]
    if (np.round(np.subtract(u, v)) == np.zeros([2, 1])).all():
        path_goal = path_goal + 1
        if path_goal == path_limit:
            goal = goal + 1
            dist_driven.append(c.distance_driven - cum_dist)
            cum_dist = c.distance_driven
            if goal == 60:
                break
            print('Goal Reached.\n'
                  'Current Goal: ', goal + 1)
            t1 = time()

            name = '\\FMS' + str(goal) + '_vel.txt'
            toa = np.loadtxt(base_dir + name, delimiter=',')
            name = '\\FMS' + str(goal) + '_toa.txt'
            vel = np.loadtxt(base_dir + name, delimiter=',')
            path2follow = update_fmm_around(t=toa, ms=vel, v=np.full(c.map_data.shape, 110),
                                            map_data=c.map_data, point_around=np.round(v).astype(np.int64),
                                            radius=15, goal_pose=np.round(beacons[goal]).astype(np.int64),
                                            current_pose=np.round(v).astype(np.int64))

            tf = tf + time() - t1
            path_goal = 1
            path_limit = np.size(path2follow, 0)
            c.draw_path(path2follow, markersize=1)
    dxi = velocity_gain * np.subtract(u, v) / np.linalg.norm(np.subtract(u, v))
    dxu = single_integrator_to_unicycle(dxi, x, 0)
    c.set_velocities(dxu)
    c.step()

print('Objective Completed')
tf = tf / 60
print('T. prom. de calculo:', tf)
print('T. total de simula.:', time() - to)

print(dist_driven)

c.finalize()
