import os
from time import time

from cormoran.cormoran import Cormoran
from cormoran.path_planners.path_planners import update_fmm_around
from cormoran.utilities.beacons_handling import *
from cormoran.utilities.transformations import *

base_dir = os.path.split(os.path.dirname(os.path.abspath(__file__)))[0]

c = Cormoran(1, initial_poses=np.array(np.mat('500.0;  500.0; 0.0')),
             map_yaml_name=base_dir + '/map/ypacarai_final.yaml',
             show_figure=False)

if not c.show_figure and False:
    c.initialize_visualization_map_only()

updated_yaml_file = base_dir + '/map/ypacarai_final.yaml'
c.set_updated_map(updated_yaml_file)

beacons = get_position_beacons(base_dir + '/map/normalizedRealBeaconsv3.json',
                               1407.6, np.zeros([2, 1]))

run_number = 0
with open(base_dir + '/map/beacon_order.txt') as f:
    lines_read = 0
    while run_number > lines_read:
        dont_print = f.readline()
        lines_read += 1
    perm_matrix = [int(i) for i in f.readline().split(",")]
    beacons = beacons[perm_matrix]

dist = [np.linalg.norm(np.array([[500], [500]]) - np.round(beacons[0])[:2])]
for i in range(59):
    dist.append(np.linalg.norm(np.round(beacons[i]) - np.round(beacons[i + 1])))

print('UFMS running')

accum_dist = 0
goal = 0
replanned = False
velocity_gain = 1
to = time()
dist_driven = []
x = c.get_poses()
c.step()
t1 = time()

name = '/FMS/FMS' + str(goal) + '_vel.txt'
toa = np.loadtxt(base_dir + name, delimiter=',', usecols=range(1000))
name = '/FMS/FMS' + str(goal) + '_toa.txt'
vel = np.loadtxt(base_dir + name, delimiter=',', usecols=range(1000))
# todo: ver 110 vs 55 ??
vel_map = np.full(c.map_data.shape, 55)
path2follow = update_fmm_around(c.map_data, np.round(x[:2]).astype(np.int64),
                                np.round(beacons[goal]).astype(np.int64),
                                np.subtract(np.round(x[:2]), np.array([1, 1])).astype(np.int64), 15, toa, vel,
                                vel_map)
tf = time() - t1

# c.draw_path(path2follow)
path_goal = 1
path_limit = np.size(path2follow, 0)
while True:
    x = c.get_poses()
    u = path2follow[path_goal, :]
    v = x[:2]
    if (np.round(np.subtract(u, v)) == np.zeros([2, 1])).all():
        path_goal = path_goal + 1
        if path_goal == path_limit:
            goal = goal + 1
            dist_driven.append(c.distance_driven - accum_dist)
            accum_dist = c.distance_driven
            if goal == 60:
                break
            # print('Goal Reached.\n',
            #       path_planning, ' Current Goal: ', goal + 1)
            t1 = time()
            name = '/FMS/FMS' + str(goal) + '_vel.txt'
            toa = np.loadtxt(base_dir + name, delimiter=',', usecols=range(1000))
            name = '/FMS/FMS' + str(goal) + '_toa.txt'
            vel = np.loadtxt(base_dir + name, delimiter=',', usecols=range(1000))
            path2follow = update_fmm_around(c.map_data, np.round(x[:2]).astype(np.int64),
                                            np.round(beacons[goal]).astype(np.int64),
                                            np.subtract(np.round(x[:2]), np.array([1, 1])).astype(np.int64), 15, toa,
                                            vel, vel_map)
            tf = tf + time() - t1
            path_goal = 1
            path_limit = np.size(path2follow, 0)
    dxi = velocity_gain * np.subtract(u, v) / np.linalg.norm(np.subtract(u, v))
    dxu = single_integrator_to_unicycle(dxi, x, 0)
    c.set_velocities(dxu)
    c.step()
# print('Crash count:', c.crash_count)
if c.crash_count == 0:
    print('Objective Completed')
else:
    print('Run finalized with errors')
tf = tf / 60 if not replanned else 0
ttot = time() - to
# print(path_planning, ' Avg. Time:', tf)
# print(path_planning, ' T. total de simula.:', time() - to)
dist_calc = []
for i in range(len(dist)):
    dist_calc.append((dist_driven[i][0] - dist[i]) / dist[i])
# guardar los datos en bruto, eliminar datos
with open(base_dir + '/results/table_fmm_{}.csv'.format(run_number), 'a') as f:
    f.write("\nUFMS,{},{},{},{}".format(tf, ttot, np.mean(dist_calc), c.crash_count))

with open(base_dir + '/simulations/run_number.txt', 'w') as f:
    f.write(str(run_number + 1))
