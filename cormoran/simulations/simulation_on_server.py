import os
from time import time

import cormoran.path_planners.path_planners as path_planners
from cormoran.cormoran import Cormoran
from cormoran.utilities.beacons_handling import *
from cormoran.utilities.transformations import *

base_dir = os.path.split(os.path.dirname(os.path.abspath(__file__)))[0]

for k in range(30):
    with open(base_dir + '/simulations/run_number.txt') as f:
        run_number = int(f.readline())
        print("-------------------------------------")
        print("Start of Run {}".format(run_number))
        print("Beacon order {} to be selected for path\n"
              "planning algorithms: A*, PF, RRT* and PP".format(run_number))
        print("-------------------------------------")

        with open(base_dir + '/results/table_{}.csv'.format(run_number), 'w') as g:
            g.write("Path Planner, T Calc, T Simul, Dist Pix, Security")

    c = Cormoran(1, initial_poses=np.array(np.mat('500.0;  500.0; 0.0')),
                 map_yaml_name=base_dir + '/map/ypacarai_final.yaml',
                 show_figure=False)
    if not c.show_figure and True:
        c.initialize_visualization_map_only()

    # np.seterr(divide='ignore', invalid='ignore')

    updated_yaml_file = base_dir + '/map/ypacarai_final.yaml'
    c.set_updated_map(updated_yaml_file)

    beacons = get_position_beacons(base_dir + '/map/normalizedRealBeaconsv3.json',
                                   1407.6, np.zeros([2, 1]))

    with open(base_dir + '/map/beacon_order.txt') as f:
        lines_read = 0
        while run_number > lines_read:
            dont_print = f.readline()
            lines_read += 1
        perm_matrix = [int(i) for i in f.readline().split(",")]
        beacons = beacons[perm_matrix]

    dist = [np.linalg.norm(np.array([[500], [500]]) - np.round(beacons[0])[:2])]
    for i in range(len(beacons) - 1):
        dist.append(np.linalg.norm(np.round(beacons[i]) - np.round(beacons[i + 1])))
    #
    for path_planning in ['a_star', 'rrt_star', 'pf']:
        accum_dist = 0
        goal = 0
        replanned = False

        print(path_planning + ' running.')
        velocity_gain = 1

        to = time()

        dist_driven = []
        x = c.get_poses()
        c.step()
        t1 = time()
        extra_data = 20 if path_planning == 'pf' else x[2]
        path2follow = getattr(path_planners, path_planning)(c.map_data,
                                                            np.round(x[:2]).astype(np.int64),
                                                            np.round(beacons[goal]).astype(np.int64),
                                                            extra_data)
        tf = time() - t1
        c.draw_path(path2follow)
        path_goal = 1
        path_limit = np.size(path2follow, 0)
        while True:
            x = c.get_poses()
            u = path2follow[path_goal, :]
            v = x[:2]
            # vR = np.round(v).astype(np.int64)
            # uR = np.round(u).astype(np.int64)
            # cLidar = c.get_lidar()
            # cMapDat = c.get_map_around(vR)
            # diff = np.logical_xor(cLidar, cMapDat)
            # if diff.any():
            #     i, j = np.where(diff)
            #     c.update_map(i, j, vR)
            # i, j = np.where(cLidar == 1)
            # if i is not []:
            #     replanned = True
            #     print("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&")
            #     print("replanned because of ??")
            #     print("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&")
            #     pt = [vR[0] - 3 + j, vR[1] + 3 - i]
            #     direction = (uR - vR)
            #     direction = (np.round(direction / np.max(np.abs(direction)))).astype(np.int64)
            #     if is_between(vR, pt, u) or c.map_data[vR[1] + direction[1][0], vR[0]] \
            #             or c.map_data[vR[1], vR[0] + direction[0][0]]:
            #         path2follow = getattr(path_planners, path_planning)(c.map_data,
            #                                                             np.round(x[:2]).astype(np.int64),
            #                                                             np.round(beacons[goal]).astype(np.int64),
            #                                                             x[2])
            #         path_limit = np.size(path2follow, 0)
            #         path_goal = 1
            #         u = path2follow[path_goal, :]

            if (np.round(np.subtract(u, v)) == np.zeros([2, 1])).all():
                path_goal = path_goal + 1
                if path_goal == path_limit:
                    goal = goal + 1
                    dist_driven.append(c.distance_driven - accum_dist)
                    accum_dist = c.distance_driven
                    if goal == 60:
                        break
                    print('Goal Reached.\n',
                          path_planning, ' Current Goal: ', goal + 1)
                    t1 = time()
                    path2follow = getattr(path_planners, path_planning)(c.map_data,
                                                                        np.round(x[:2]).astype(np.int64),
                                                                        np.round(beacons[goal]).astype(np.int64),
                                                                        extra_data)
                    tf = tf + time() - t1
                    c.draw_path(path2follow)
                    path_goal = 1
                    path_limit = np.size(path2follow, 0)
            dxi = velocity_gain * np.subtract(u, v) / np.linalg.norm(np.subtract(u, v))

            dxu = single_integrator_to_unicycle(dxi, x, 0)

            c.set_velocities(dxu)
            c.step()
        # print('Crash count:', c.crash_count)
        if c.crash_count == 0:
            print('Objective Completed')
        else:
            print('Run finalized with errors')

        tf = tf / 60 if not replanned else 0
        ttot = time() - to
        # print(path_planning, ' Avg. Time:', tf)
        # print(path_planning, ' T. total de simula.:', time() - to)
        dist_calc = []
        for i in range(len(dist)):
            dist_calc.append((dist_driven[i][0] - dist[i]) / dist[i])
        # guardar los datos en bruto, eliminar datos
        with open(base_dir + '/results/table_{}.csv'.format(run_number), 'a') as f:
            f.write("\n{},{},{},{},{}".format(path_planning, tf, ttot, np.mean(dist_calc), c.crash_count))

        c = Cormoran(1, initial_poses=np.array(np.mat('500.0;  500.0; 0.0')),
                     map_yaml_name=base_dir + '/map/ypacarai_final.yaml',
                     show_figure=False)
        if not c.show_figure and False:
            c.initialize_visualization_map_only()

        updated_yaml_file = base_dir + '/map/ypacarai_final.yaml'
        c.set_updated_map(updated_yaml_file)

    # obtener los datos en bruto, obtener koalification en escala
    with open(base_dir + '/results/table_{}.csv'.format(run_number), 'r') as f:
        _ = f.readline()
        astar_data = f.readline().split(',')
        pf_data = f.readline().split(',')
        rrt_star_data = f.readline().split(',')
        pp_data = f.readline().split(',')

        max_tcalc = max(pp_data[1], astar_data[1], pf_data[1], rrt_star_data[1])
        min_tcalc = min(pp_data[1], astar_data[1], pf_data[1], rrt_star_data[1])
        max_tsim = max(pp_data[2], astar_data[2], pf_data[2], rrt_star_data[2])
        min_tsim = min(pp_data[2], astar_data[2], pf_data[2], rrt_star_data[2])
        max_dist = max(pp_data[3], astar_data[3], pf_data[3], rrt_star_data[3])
        min_dist = min(pp_data[3], astar_data[3], pf_data[3], rrt_star_data[3])

    with open(base_dir + '/results/table_{}.csv'.format(run_number), 'a') as f:
        f.write('\n')
        f.write('\nMAX,{},{},{}'.format(max_tcalc, max_tsim, max_dist))
        f.write('\nMIN,{},{},{}'.format(min_tcalc, min_tsim, min_dist))

    with open(base_dir + '/simulations/run_number.txt', 'w') as f:
        f.write(str(run_number + 1))
