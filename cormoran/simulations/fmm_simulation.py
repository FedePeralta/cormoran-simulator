from cormoran.cormoran import Cormoran
from cormoran.utilities.beacons_handling import *
from cormoran.utilities.transformations import *
from cormoran.path_planners.path_planners import fmm

import numpy as np
from time import time

c = Cormoran(1, initial_poses=np.array(np.mat('500.0;  500.0; 0.0')),
             map_yaml_name='E:\\LSD\\177\\code\\python\\cormoran\\cormoran\\map\\ypacarai_final.yaml',
             show_figure=False)
if not c.show_figure and False:
    c.initialize_visualization_map_only()
beacons = get_position_beacons('E:\\LSD\\177\\code\\python\\cormoran\\cormoran\\map'
                               '\\normalizedRealBeaconsv3.json', 1407.6, np.zeros([2, 1]))
perm_matrix = [25, 59, 26, 7, 44, 2, 42, 55, 21, 30, 53, 22,
               52, 11, 51, 20, 50, 18, 31, 8, 33, 10, 45, 54, 43, 48, 39,
               4, 46, 37, 38, 1, 32, 3, 19, 49, 14, 58, 9, 41, 5, 24, 36,
               12, 47, 6, 35, 17, 57, 23, 16, 29, 13, 56, 40, 0, 28, 27, 15, 34]
beacons = beacons[perm_matrix]

dist = [np.linalg.norm(np.array([[500], [500]])-np.round(beacons[0])[:2])]
for i in range(len(beacons)-1):
    dist.append(np.linalg.norm(np.round(beacons[i])-np.round(beacons[i+1])))

goal = 0
print('Current Goal: ', goal+1)
c.draw_points(beacons)
velocity_gain = 1

x = c.get_poses()
c.step()
to = time()

dist_driven = []
cum_dist = 0

v = np.round(x[:2]).astype(np.int64)
# [v[1][0]-100:v[1][0]+101, v[0][0]-100:v[0][0]+101]
path2follow, vel_map, toa = fmm(c.map_data, v, x[2], np.round(beacons[goal]).astype(np.int64), fmm_type=1)

velocities = [vel_map]

toas = [toa]

print(path2follow)
tf = time()-to
path_goal = 1
path_limit = np.size(path2follow, 0)
c.draw_path(path2follow)
while True:
    x = c.get_poses()

    u = path2follow[path_goal, :]
    v = x[:2]
    if (np.round(np.subtract(u, v)) == np.zeros([2, 1])).all():
        path_goal = path_goal + 1
        if path_goal == path_limit:
            goal = goal + 1
            dist_driven.append(c.distance_driven - cum_dist)
            cum_dist = c.distance_driven
            if goal == 60:
                break
            print('Goal Reached.\n'
                  'Current Goal: ', goal + 1)
            t1 = time()
            path2follow, vel_map, toa  = fmm(c.map_data, np.round(x[:2]).astype(np.int64), x[2],
                                 np.round(beacons[goal]).astype(np.int64), 0)
            velocities = [vel_map]

            toas = [toa]
            tf = tf + time() - t1
            path_goal = 1
            path_limit = np.size(path2follow, 0)
    dxi = velocity_gain * np.subtract(u, v) / np.linalg.norm(np.subtract(u, v))
    dxu = single_integrator_to_unicycle(dxi, x, 0)
    c.set_velocities(dxu)
    c.step()

with open('toas.txt', 'w') as f:
    for item in toas:
        f.write("%s\n" % item)

with open('velocities.txt', 'w') as f:
    for item in velocities:
        f.write("%s\n" % item)

print('Objective Completed')
tf = tf/60
print('T. prom. de calculo:', tf)
print('T. total de simula.:', time() - to)

print(dist_driven)
print(dist)

c.finalize()
