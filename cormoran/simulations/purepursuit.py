from time import time

from cormoran.cormoran import Cormoran
from cormoran.utilities.beacons_handling import *
from cormoran.utilities.transformations import *

c = Cormoran(1, initial_poses=np.array(np.mat('500.0; 500.0; 0.0')),
             map_yaml_name='E:\\LSD\\177\\code\\python\\cormoran\\cormoran\\map\\ypacarai_final.yaml',
			 show_figure=False)
beacons = get_position_beacons('E:\\LSD\\177\\code\\python\\cormoran\\cormoran\\map'
                               '\\normalizedRealBeaconsv3.json', 1407.6, np.zeros([2, 1]))
perm_matrix = [25, 59, 26, 7, 44, 2, 42, 55, 21, 30, 53, 22,
               52, 11, 51, 20, 50, 18, 31, 8, 33, 10, 45, 54, 43, 48, 39,
               4, 46, 37, 38, 1, 32, 3, 19, 49, 14, 58, 9, 41, 5, 24, 36,
               12, 47, 6, 35, 17, 57, 23, 16, 29, 13, 56, 40, 0, 28, 27, 15, 34]
beacons = beacons[perm_matrix]

dist = [np.linalg.norm(np.array([[500], [500]]) - np.round(beacons[0])[:2])]
for i in range(len(beacons) - 1):
    dist.append(np.linalg.norm(np.round(beacons[i]) - np.round(beacons[i + 1])))

to = time()

dist_driven = []
cum_dist = 0

goal = 0
print('Current Goal: ', goal + 1)
c.draw_points(beacons)
velocity_gain = 1
while True:
    # Get poses of agents
    x = c.get_poses()

    u = beacons[goal]
    v = x[:2]
    if (np.round(np.subtract(u, v)) == np.zeros([2, 1])).all():
        c.draw_path(beacons[goal:goal + 2])
        goal = goal + 1
        dist_driven.append(c.distance_driven - cum_dist)
        cum_dist = c.distance_driven
        if goal == 60:
            break
        print('Goal Reached.\n'
              'Current Goal: ', goal + 1)

    dxi = velocity_gain * np.subtract(u, v) / np.linalg.norm(np.subtract(u, v))
    dxu = single_integrator_to_unicycle(dxi, x, 1)
    c.set_velocities(dxu)
    c.step()
print('Objective Completed')
print('T. total de simula.:', time() - to)

print(dist_driven)
print(dist)

c.finalize()
