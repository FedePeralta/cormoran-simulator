# Cormoran Simulator
Por medio de un simple programa, simule un ASV capaz de recorrer en un mapa. Simulador del Vehículo Autónomo de Superficie del
LSD desarrollado en lenguaje Python.

Este simulador posee detección de colisiones, soporte para guardar datos, medidor de distancia
recorrida y otras especificaciones.

## Instalación
Para descargar la libreria debe clonar este repositorio mediante:
```
git clone https://bitbucket.org/FedePeralta/cormoran-simulator
```

### Dependencias
Para utilizar el simulador, se deben instalar los siguientes paquetes:

* scipy
* numpy
* matplotlib
* pyyaml

Si no posee estos paquetes, se descarga e instala mediante el comando:
```
pip install scipy numpy matplotlib pyyaml
```
Para instalar por primera vez el paquete Cormoran dirígase a la direccion de la carpeta principal e ingrese:

```
python setup.py install
``` 
En caso de que el paquete ya haya sido instalado, debe desinstalar el paquete e instalarlo de nuevo
```
pip uninstall cormoran-simulator
python setup.py install
``` 


##Uso
Luego, se usa el paquete cormoran importando e instanciándolo de la siguiente forma:
```pythonstub
from cormoran.cormoran import *
c = Cormoran(number_of_agents, save_data, map_yaml_name, initial_poses,
            initial_lidar, show_figure, boundaries)
``` 
en donde el argumento indispensable es `map_yaml_name`, ubicación de un archivo .yaml, el cuál
tiene datos sobre el nombre de la imagen y otros. La imagen en .png (de preferencia 1000x1500) debe estar contenida en la
misma carpeta que el .yaml.

#### Archivo YAML
Por ejemplo, para:

![image](https://bitbucket.org/FedePeralta/cormoran-simulator/raw/8002cfe236da1345bfc1df88ea4483b876809937/files.jpg)

```python
map_yaml_name = '<path_to_177>\\177\\code\\python\\cormoran\\cormoran\\map\\ypacarai\\ypacarai_final.yaml'
```
Donde `<>` es el directorio hasta la carpeta `177`.
Este archivo .yaml posee la siguiente estructura:
```
image: ypacarai_final.png
resolution: 10.33
origin: [200.0, 500.0, 0.0]
occupied_thresh: 0.4
free_thresh: 0.05
negate: 1
```
`image` indica el nombre de la imagen a adquirir para la simulación, `resolution` es la resolución de 1 pixel en metros,
 en este caso 1 px = 10.33 m, `negate` indica si la imagen debe complementarse, ya que el programa toma al rgb =
 `[0 0 0]` como un pixel ocupado. Aunque los otros terminos `origin`, `occupied_thresh`, `free_thresh` no estan todavia
 desarrollados en este simulador, se los agrega debido a que se utilizan en ROS, candidato a ser utilizado en futuros
 trabajos. 

#### Poses o Posiciones
Si se quiere definir las poses inciales`{x, y, theta} [m, m, rad]` iniciales, se debe modificar el argumento
`initial_poses=numpy.array(numpy.mat('x; y; t'))`, obteniendo una vector de la  forma `[[x], [y], [theta]]`. Donde x, y
indican las posiciones respecto al origen de la imagen, y t el angulo de direccion del ASV respecto al eje horizontal.

Luego para acceder a las poses durante la ejecución del script se debe ingresar `c.get_poses()`, función que retorna las posiciones actuales del ASV.
Este método solo se puede llamar una vez por frame, es decir, después de obtener las poses, en algún momento debe haber
un `c.step()` para habilitar nuevamente la solicitud de obtener las poses.

#### Velocidades
Para definir velocidades se utiliza la función `c.set_velocities(dxu)`, vector de velocidades: lineal y angular
`[[v] [w]]`. Si el usuario esta acostumbrado a utilizar velocidades cartesianas `[[vx] [vy]]` (Velocidades Single
Integator), se puede utilizar:

```python
from cormoran.utilities.transformations import *
dxu = single_integrator_to_unicycle(dxi, p)
``` 
indicando la velocidad `dxi = [[vx] [vy]]`, y las poses actuales `p = [[x] [y] [t]]`.
## Ejemplos
Puede encontrar ejemplos en la carpeta [/simulations](https://bitbucket.org/FedePeralta/cormoran-simulator/src/88fc02f7c489d2f0fe0a6d1b9eba2972464d9e88/cormoran/simulations/).
Estos ejemplos incluyen:

* Demo: Simulador basico que viaja entre 2 puntos en bucle.
* Pure Pursuit: Simulador que recorre utilizando el `Pure Pursuit`.
* A star simulation: Simulador que recorre utilizando el `A*`.
* A star online simulation: Utilizando `A*`, se recorre un mapa con obstaculos no conocidos.

## Estado
Este proyecto se encuentra en beta, por lo que deberia funcionar sin errores, aunque se recomienda actualizar constantemente.