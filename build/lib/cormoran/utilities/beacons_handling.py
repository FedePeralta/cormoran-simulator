import json

import numpy as np


def get_position_beacons(json_name, scale=1, offset=np.zeros([2, 1])):
    with open(json_name) as f:
        data = json.load(f)
        xpos = data['x1 ']['xRel'] * scale - offset[0]
        ypos = data['x1 ']['yRel'] * scale - offset[1]
        beacons = np.array([xpos, ypos])
        beacons = np.expand_dims(beacons, 0)
        for i in range(len(data) - 1):
            name = 'x' + str(i + 2) + ' '
            xpos = data[name]['xRel'] * scale - offset[0]
            ypos = data[name]['yRel'] * scale - offset[1]
            beacons = np.concatenate([beacons, [np.array([xpos, ypos])]])

        return beacons
