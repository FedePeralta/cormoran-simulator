import random
import time
from heapq import *

import numpy as np
import scipy as sp
import scipy.ndimage


def gradient_descent(t, current_pose, goal_pose):
    [grady, gradx] = np.gradient(t)
    precision = max(abs(gradx[goal_pose[1], goal_pose[0]]), abs(grady[goal_pose[1], goal_pose[0]]))[0]
    precision = precision * (1 + 0.1)
    previous_step_size = 1

    path2follow = np.array([np.array(current_pose[0]), np.array(current_pose[1])])
    path2follow = np.expand_dims(path2follow, 0)

    while previous_step_size > precision:
        last_added = path2follow[len(path2follow) - 1, :]

        to_add = np.array([last_added[0] - gradx[current_pose[1], current_pose[0]],
                           last_added[1] - grady[current_pose[1], current_pose[0]]])
        path2follow = np.concatenate([path2follow, [to_add]])
        current_pose = np.round(to_add).astype(np.int64)
        previous_step_size = max(abs(last_added - to_add))[0]

    return path2follow


def pf_gradient_descent(t, current_pose, goal_pose):
    [grady, gradx] = np.gradient(50000 * t / np.max(t))

    precision = max(abs(gradx[goal_pose[1], goal_pose[0]]), abs(grady[goal_pose[1], goal_pose[0]]))
    precision = precision * (1 + 0.1)
    previous_step_size = 1

    path2follow = np.array([np.array(current_pose[0]), np.array(current_pose[1])])
    path2follow = np.expand_dims(path2follow, 0)

    while previous_step_size > precision:
        last_added = path2follow[len(path2follow) - 1, :]

        to_add = np.array([last_added[0] - gradx[current_pose[1], current_pose[0]],
                           last_added[1] - grady[current_pose[1], current_pose[0]]])
        path2follow = np.concatenate([path2follow, [to_add]])
        current_pose = np.round(to_add).astype(np.int64)
        previous_step_size = max(abs(last_added - to_add))

    return path2follow


def calc_final_path(closed_set, goal_pos):
    path2follow = np.array([np.array([goal_pos[0]]), np.array([goal_pos[1]])])
    path2follow = np.expand_dims(path2follow, 0)
    while goal_pos is not None:
        key = str(goal_pos[0]) + "X" + str(goal_pos[1])
        if closed_set[key][1] is None:
            break
        to_add = np.array([np.array([closed_set[key][1][0]]), np.array([closed_set[key][1][1]])])
        path2follow = np.concatenate([path2follow, [to_add]])
        goal_pos = closed_set[key][1]

    return path2follow


def simplify_path(path):
    prev_dir = [2, 2]
    new_path = []
    for i in range(len(path) - 1):
        next_dir = [path[i + 1][0] - path[i][0], path[i + 1][1] - path[i][1]]
        if next_dir[0] != prev_dir[0] or next_dir[1] != prev_dir[1]:
            to_add = path[i]
            if i == 0:
                new_path = to_add
                new_path = np.expand_dims(new_path, 0)
            else:
                new_path = np.concatenate([new_path, [to_add]])
        prev_dir = next_dir

    new_path = np.concatenate([new_path, [path[len(path) - 1]]])
    return np.flipud(new_path)


def get_neighbors(my_pos, map_dat):
    cool_matrix = map_dat[my_pos[1] - 1:my_pos[1] + 2,
                          my_pos[0] - 1:my_pos[0] + 2]
    all_my_neighbors = []
    if cool_matrix[0, 2] == 0:
        all_my_neighbors.append([my_pos[0] + 1, my_pos[1] - 1])
    if cool_matrix[1, 2] == 0:
        all_my_neighbors.append([my_pos[0] + 1, my_pos[1]])
    if cool_matrix[2, 2] == 0:
        all_my_neighbors.append([my_pos[0] + 1, my_pos[1] + 1])
    if cool_matrix[0, 1] == 0:
        all_my_neighbors.append([my_pos[0], my_pos[1] - 1])
    if cool_matrix[2, 1] == 0:
        all_my_neighbors.append([my_pos[0], my_pos[1] + 1])
    if cool_matrix[0, 0] == 0:
        all_my_neighbors.append([my_pos[0] - 1, my_pos[1] - 1])
    if cool_matrix[1, 0] == 0:
        all_my_neighbors.append([my_pos[0] - 1, my_pos[1]])
    if cool_matrix[2, 0] == 0:
        all_my_neighbors.append([my_pos[0] - 1, my_pos[1] + 1])
    return all_my_neighbors


def get_first_neighbors(my_pos, map_dat, heading_angle):
    left_lim = heading_angle + 1.58
    right_lim = heading_angle - 1.58
    cool_matrix = map_dat[my_pos[1] - 1:my_pos[1] + 2,
                          my_pos[0] - 1:my_pos[0] + 2]
    all_my_neighbors = []
    if cool_matrix[0, 2] == 0 and right_lim < -0.785 < left_lim:
        all_my_neighbors.append([my_pos[0] + 1, my_pos[1] - 1])

    if cool_matrix[1, 2] == 0 and right_lim < 0 < left_lim:
        all_my_neighbors.append([my_pos[0] + 1, my_pos[1]])

    if cool_matrix[2, 2] == 0 and right_lim < 0.785 < left_lim:
        all_my_neighbors.append([my_pos[0] + 1, my_pos[1] + 1])

    if cool_matrix[0, 1] == 0 and right_lim < -1.5708 < left_lim:
        all_my_neighbors.append([my_pos[0], my_pos[1] - 1])

    if cool_matrix[2, 1] == 0 and right_lim < 1.5708 < left_lim:
        all_my_neighbors.append([my_pos[0], my_pos[1] + 1])

    if cool_matrix[0, 0] == 0 and right_lim < -2.3562 < left_lim:
        all_my_neighbors.append([my_pos[0] - 1, my_pos[1] - 1])

    if cool_matrix[1, 0] == 0 and right_lim < 3.1416 < left_lim:
        all_my_neighbors.append([my_pos[0] - 1, my_pos[1]])

    if cool_matrix[2, 0] == 0 and right_lim < 2.3562 < left_lim:
        all_my_neighbors.append([my_pos[0] - 1, my_pos[1] + 1])

    return all_my_neighbors


def get_adjacency_neighbors(my_pos, x_lim, y_lim):
    # directions= North South East West
    directions = [True, True, True, True]
    if my_pos[1] == 0:
        directions[1] = False
    if my_pos[0] == 0:
        directions[3] = False
    if my_pos[1] + 1 == y_lim:
        directions[0] = False
    if my_pos[0] + 1 == x_lim:
        directions[2] = False

    all_my_neighbors = []
    if directions[0]:
        all_my_neighbors.append([my_pos[0], my_pos[1] + 1])
    if directions[1]:
        all_my_neighbors.append([my_pos[0], my_pos[1] - 1])
    if directions[2]:
        all_my_neighbors.append([my_pos[0] + 1, my_pos[1]])
    if directions[3]:
        all_my_neighbors.append([my_pos[0] - 1, my_pos[1]])
    return all_my_neighbors


def get_cost(my_node, next_pose, map_data):
    direction = abs(next_pose[0] - my_node[0]) + abs(next_pose[1] - my_node[1])
    if not map_data[next_pose[1], next_pose[0]] == 0:
        cost = None
    elif direction == 2:
        if not map_data[next_pose[1], my_node[0]] == 0:
            cost = None
        elif not map_data[my_node[1], next_pose[0]] == 0:
            cost = None
        else:
            cost = 1.404
    else:
        cost = 1
    return cost


def attractive_potential(x, y, g_p):
    return 2.5 * ((x - g_p[0]) ** 2 + (y - g_p[1]) ** 2)


def repulsive_potential(x, y, map_data, eta, threshold, g_pos):
    if map_data[y, x] == 1:
        return eta

    if threshold == 0 or (g_pos[0] == x and g_pos[1] == y):
        return 0

    matrix_around = map_data[y - threshold:y + threshold, x - threshold:x + threshold]
    sum_of_obs = np.sum(matrix_around)
    size_sq = np.size(matrix_around, 0) ** 2
    if sum_of_obs == size_sq:
        return 0
    else:
        return eta * (sum_of_obs / size_sq)


# todo: cuidar que esto cambia
def a_star(map_data, start_pose, goal_pose, heading_angle):
    c_pose = [start_pose[0][0], start_pose[1][0]]
    g_pose = [goal_pose[0][0], goal_pose[1][0]]
    fix_heading = True
    queue = []
    heappush(queue, (1, c_pose, 0))
    closed_set = dict()
    key = str(c_pose[0]) + "X" + str(c_pose[1])
    closed_set[key] = [0, None]
    goal_pos = []
    while len(queue) > 0:
        current_pos = heappop(queue)
        if current_pos[1] == g_pose:
            goal_pos = current_pos[1]
            break

        if not fix_heading:
            neighbors_of = get_neighbors(current_pos[1], map_data)
        else:
            fix_heading = False
            if heading_angle is not None:
                neighbors_of = get_first_neighbors(current_pos[1], map_data, heading_angle)
            else:
                neighbors_of = get_neighbors(current_pos[1], map_data)
        for i in range(len(neighbors_of)):
            next_pos = neighbors_of[i]
            next_cost = get_cost(current_pos[1], next_pos, map_data)
            if next_cost is not None:
                next_cost = current_pos[2] + next_cost
                key = str(next_pos[0]) + "X" + str(next_pos[1])
                if key not in closed_set or closed_set[key][0] > next_cost:
                    closed_set[key] = [next_cost, current_pos[1]]
                    priority = next_cost + abs(g_pose[0] - next_pos[0]) + abs(g_pose[1] - next_pos[1])
                    heappush(queue, (priority, next_pos, next_cost))
    if not goal_pos:
        if heading_angle is not None:
            print('No se ha encontrado una ruta, probando sin restricciones de movimiento')
            return a_star(map_data, start_pose, goal_pose, None)
        else:
            print('Critical error. Path not found.')
            return None

    if heading_angle is None:
        print('Ruta sin restricciones de movimiento encontrada.')
    path2follow = calc_final_path(closed_set, goal_pos)
    path2follow = simplify_path(path2follow)
    return path2follow


def pf(map_data, current_pose, goal_pose, threshold=10):
    goal_pos = [int(round(goal_pose[0][0])), int(round(goal_pose[1][0]))]
    c_pos = [int(round(current_pose[0][0])), int(round(current_pose[1][0]))]
    p_map = np.zeros(map_data.shape)
    max_x = np.size(map_data, 1)
    max_y = np.size(map_data, 0)

    for x in range(max_x):
        for y in range(max_y):
            p_map[y, x] = attractive_potential(x, y, goal_pos)
    max_ug = np.max(p_map)
    for x in range(max_x):
        for y in range(max_y):
            uo = repulsive_potential(x, y, map_data, max_ug, threshold, goal_pos)
            p_map[y, x] = p_map[y, x] + uo
            if p_map[y, x] > max_ug:
                p_map[y, x] = max_ug
    # plt.imshow(p_map, cmap='hot')
    # plt.show()
    # figure.canvas.draw()
    path2follow = pf_gradient_descent(p_map, c_pos, goal_pos)
    return path2follow


def cost_update(me, toa, v, x_lim, y_lim):
    if v[me[1], me[0]] == 0:
        t = np.inf
    else:
        # up
        if me[1] + 1 == y_lim:
            t2u = np.inf
        else:
            t2u = toa[me[1] + 1, me[0]]
        # dn
        if me[1] == 0:
            t2d = np.inf
        else:
            t2d = toa[me[1] - 1, me[0]]
        # lf
        if me[0] == 0:
            t1l = np.inf
        else:
            t1l = toa[me[1], me[0] - 1]
        # rt
        if me[0] + 1 == x_lim:
            t1r = np.inf
        else:
            t1r = toa[me[1], me[0] + 1]

        t1 = min(t1l, t1r)
        t2 = min(t2d, t2u)
        if (not np.equal(t1, 1000000)) and np.equal(t2, 1000000):
            t = t1 + 1 / v[me[1], me[0]]
        elif (not np.equal(t2, 1000000)) and np.equal(t1, 1000000):
            t = t2 + 1 / v[me[1], me[0]]
        else:
            sqrtarg = np.abs(-1 * np.power(t1 - t2, 2) + 2 / np.power(v[me[1], me[0]], 2))
            t = ((t1 + t2) + np.sqrt(sqrtarg)) / 2
            if t > 1000000:
                t = 1000000
    return t


def lowest_in_trial(queue, trial_cost):
    lowest = queue[0]
    for data in queue:
        if trial_cost[data[1][1], data[1][0]] < trial_cost[lowest[1][1], lowest[1][0]]:
            lowest = data

    return lowest


def perform_fmm(current_pose, v):
    # toa : Time of Arrival Matrix
    toa = np.full(v.shape, 1000000.0)
    x_lim = np.size(v, 1)
    y_lim = np.size(v, 0)
    # T (startPoint) ← 0
    c_pose = [current_pose[0][0], current_pose[1][0]]
    toa[c_pose[1], c_pose[0]] = 0

    # info_points tiene -1 para Far, 0 para Trial, 1 para Known
    # Far ← all grid points
    # # info_points = np.full(map_data.shape, -1)
    # Known ← all grid points with known cost
    # # info_points[c_pose[1], c_pose[0]] = 1
    trial_queue = []
    # for each adjacent a of Known point do
    if np.size(c_pose) < 3:
        for neighbor in get_adjacency_neighbors(c_pose, x_lim, y_lim):
            # # ,
            # # map_data[c_pose[1] - 1:c_pose[1] + 2,
            # # c_pose[0] - 1:c_pose[0] + 2]
            # Trial ← a
            # # info_points[neighbor[1], neighbor[0]] = 0
            cost = cost_update(neighbor, toa, v, x_lim, y_lim)
            heappush(trial_queue, (cost, neighbor))
            # T(a) = costUpdate(a)
            toa[neighbor[1], neighbor[0]] = cost
            # end for
    else:
        for i in range(len(c_pose[0])):
            obs = [c_pose[0][i], c_pose[1][i]]
            for neighbor in get_adjacency_neighbors(obs, x_lim, y_lim):
                if toa[neighbor[1], neighbor[0]] > 0:
                    cost = cost_update(neighbor, toa, v, x_lim, y_lim)
                    heappush(trial_queue, (cost, neighbor))
                    toa[neighbor[1], neighbor[0]] = cost

    # # t_en_y, t_en_x = np.where(info_points == 0)
    # while Trial is not empty do
    # # while len(t_en_y) > 0:
    while len(trial_queue) > 0:
        # p ← point with lowest cost in Trial
        # # next_point = lowest_in_trial(t_en_x, t_en_y, toa)
        next_point = lowest_in_trial(trial_queue, toa)
        trial_queue.remove(next_point)
        if next_point[0] == np.inf:
            break
        next_point = next_point[1]
        # remove p point from Trial
        # Known ← p
        # # info_points[next_point[1], next_point[0]] = 1
        # for each neighbor point a of p do
        # # # if map_data[next_point[1], next_point[0]] == 1:
        # # #     info_points[next_point[1], next_point[0]] = 1
        # # #     toa[next_point[1], next_point[0]] = np.inf

        for neighbor in get_adjacency_neighbors(next_point, x_lim, y_lim):
            if toa[neighbor[1], neighbor[0]] == 1000000:
                # #  and info_points[neighbor[1], neighbor[0]] != 1:
                # T(a) = costUpdate(a)
                cost = cost_update(neighbor, toa, v, x_lim, y_lim)
                heappush(trial_queue, (cost, neighbor))
                #     cost = cost * 1.01
                toa[neighbor[1], neighbor[0]] = cost
                # if a ∈ Far then
                # # if info_points[neighbor[1], neighbor[0]] == -1:
                # #     # remove a from Far
                # #     # Trial ← a
                # #     info_points[neighbor[1], neighbor[0]] = 0
                # #     # end if
            # end for
        # # t_en_y, t_en_x = np.where(info_points == 0)
        # end while
    # return T
    return toa


# todo cuidar que esto cambio
def fmm(map_data, current_pose, goal_pose, heading_angle, fmm_type):
    # Para hacer FMM:
    if fmm_type == 0:
        # Calculate speed matrix V from M
        v = np.full(map_data.shape, 1)
        i, j = np.where(map_data == 1)
        v[i, j] = 0

        # Arrival time matrix (T) ← FMM (V, pstart, pend)
        t = perform_fmm(goal_pose, v)

        i, j = np.where(t >= 99999)
        ma = np.max(t[t < 99999])
        t[i, j] = ma
        # path ← gradientDescent(T, pstart, pend)
        path2follow = gradient_descent(t, current_pose, goal_pose)

        ms = -1
        tobs = -1
        tmain = -1

    # Para hacer FMS:
    elif fmm_type == 1:
        v = np.full(map_data.shape, 110)

        i, j = np.where(map_data == 1)
        v[i, j] = 0
        # for each point a in obstacle area in M do
        # obstaclePoints ← obstaclePoints+a
        # end for
        # Ms, ← FMM (M, obstaclePoints)
        print('lol')
        print('Creating obstacle fmm')
        ms = perform_fmm(np.array([[j], [i]]), v)
        print('\nCreating main fmm')
        # TFMS, ← FMM (Ms, pstart)
        # ms = ms[1:np.size(ms, 1)-1, 1:np.size(ms, 0)-1]
        t = perform_fmm(goal_pose, ms)
        print('\ndone')
        i, j = np.where(t >= 99999)
        ma = np.max(t[t < 99999])
        t[i, j] = ma
        i, j = np.where(t == np.inf)
        t[i, j] = ma
        # path ← gradientDescent(TFMS, pstart, pend)
        path2follow = gradient_descent(t, current_pose, goal_pose)

    else:
        print(heading_angle)
        path2follow = []
        t = -1
        v = -1
        ms = -1
        tobs = -1
        tmain = -1

    return path2follow, t, ms # , v, tobs, tmain, totaltime


def update_fmm(toa, v, point_around, radius, current_pose):
    trial_queue = []
    x_lim = np.size(v, 1)
    y_lim = np.size(v, 0)
    c_pose = [point_around[0][0], point_around[1][0]]
    y, x = np.ogrid[-c_pose[1]:y_lim - c_pose[1], -c_pose[0]:x_lim - c_pose[0]]
    mask = x * x + y * y <= radius * radius
    toa[mask] = 1000000.0
    toa[c_pose[1], c_pose[0]] = 0

    if np.size(current_pose) > 1:
        toa[c_pose[1], c_pose[0]] = 1000000.0
        c_pose = [current_pose[0][0], current_pose[1][0]]
        # toa[c_pose[1], c_pose[0]] = 0

    #         i, j = np.where(toa == 1000000.0)
    #         for k in range(len(i)):
    #             obs = [j[k], i[k]]
    #             heappush(trial_queue, (1000000.0, obs))
    #
    #     else:
    if np.size(c_pose) < 3:
        for neighbor in get_adjacency_neighbors(c_pose, x_lim, y_lim):
            cost = cost_update(neighbor, toa, v, x_lim, y_lim)
            heappush(trial_queue, (cost, neighbor))
            toa[neighbor[1], neighbor[0]] = cost
    else:
        for i in range(len(c_pose[0])):
            obs = [c_pose[0][i], c_pose[1][i]]
            for neighbor in get_adjacency_neighbors(obs, x_lim, y_lim):
                if toa[neighbor[1], neighbor[0]] > 0:
                    cost = cost_update(neighbor, toa, v, x_lim, y_lim)
                    heappush(trial_queue, (cost, neighbor))
                    toa[neighbor[1], neighbor[0]] = cost

    while len(trial_queue) > 0:
        next_point = lowest_in_trial(trial_queue, toa)
        trial_queue.remove(next_point)
        if next_point[0] == np.inf:
            break
        next_point = next_point[1]

        for neighbor in get_adjacency_neighbors(next_point, x_lim, y_lim):

            if toa[neighbor[1], neighbor[0]] == 1000000:
                cost = cost_update(neighbor, toa, v, x_lim, y_lim)
                heappush(trial_queue, (cost, neighbor))
                toa[neighbor[1], neighbor[0]] = cost

    return toa


def update_fmm_around(t, ms, v, map_data, point_around, radius, goal_pose, current_pose):
    # totaltime = time.time()
    i, j = np.where(map_data == 1)
    v[i, j] = 0
    # print('Updating obstacle fmm')
    # t1 = time.time()
    ms = update_fmm(ms, v, point_around, radius, -1)

    # Apply gaussian filter
    sigma = [4, 4]
    ms = sp.ndimage.filters.gaussian_filter(ms, sigma)

    # tobs = time.time() - t1
    # print(tobs, '\nUpdating main fmm')
    # t1 = time.time()
    t = update_fmm(t, ms, point_around, radius, current_pose)

    # tmain = time.time() - t1
    # print(tmain, '\ndone')

    i, j = np.where(t >= 99999)
    ma = np.max(t[t < 99999])
    t[i, j] = ma

    # Apply gaussian filter
    sigma = [1, 1]
    t = sp.ndimage.filters.gaussian_filter(t, sigma)

    path2follow = gradient_descent(t, current_pose, goal_pose)

    # totaltime = time.time() - totaltime
    return path2follow  # , t, ms, v, tobs, tmain, totaltime


class Node:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.parent = None


def get_nearest_index(nodes_list, rnd):
    dlist = [(node.x - rnd[0]) ** 2 + (node.y - rnd[1])
             ** 2 for node in nodes_list]
    minind = dlist.index(min(dlist))
    return minind


def collision_check(new_node, parent_node, map_data):
    rounded_nn_x = int(round(new_node.x))
    rounded_nn_y = int(round(new_node.y))
    rounded_pn_x = int(round(parent_node.x))
    rounded_pn_y = int(round(parent_node.y))

    if map_data[rounded_nn_y, rounded_nn_x] == 1:
        return False

    m = (new_node.y - parent_node.y) / (new_node.x - parent_node.x)
    b = new_node.y - m * new_node.x
    if rounded_nn_x < rounded_pn_x:
        min_x = rounded_nn_x
        max_x = rounded_pn_x
    else:
        min_x = rounded_pn_x
        max_x = rounded_nn_x
    if rounded_nn_y < rounded_pn_y:
        min_y = rounded_nn_y
        max_y = rounded_pn_y
    else:
        min_y = rounded_pn_y
        max_y = rounded_nn_y

    bounds = map_data[min_y:max_y + 1, min_x:max_x + 1]
    i, j = np.where(bounds == 1)
    for k in range(len(i)):
        if i[k] + min_y == int(round(m * (min_x + j[k]) + b)):
            print(' crosses obstacle')
            return False
    return True


def rrt_star(map_data, current_pose, goal_pose, _):
    c_pose = Node(current_pose[0][0], current_pose[1][0])
    g_pose = Node(goal_pose[0][0], goal_pose[1][0])

    node_list = [c_pose]
    y_max, x_max = map_data.shape

    while True:

        # switch randomly to random or goal as point
        if random.randint(0, 100) > 40:
            rnd = [random.uniform(0, x_max), random.uniform(
                0, y_max)]
        else:
            rnd = [g_pose.x, g_pose.y]

        # Find nearest node
        nind = get_nearest_index(node_list, rnd)
        nearest_node = node_list[nind]
        # steering
        theta = np.math.atan2(rnd[1] - nearest_node.y, rnd[0] - nearest_node.x)

        new_node = Node(node_list[nind].x + random.randint(5, 50) * np.math.cos(theta),
                        node_list[nind].y + random.randint(5, 50) * np.math.sin(theta))
        new_node.parent = nind

        # print(new_node.x, ' ', new_node.y)
        if not collision_check(new_node, node_list[nind], map_data):
            continue

        node_list.append(new_node)
        # check goal
        dx = new_node.x - g_pose.x
        dy = new_node.y - g_pose.y
        d = np.math.sqrt(dx * dx + dy * dy)
        if d <= 50 and collision_check(g_pose, new_node, map_data):
            break

        # plt.plot([new_node.x, node_list[new_node.parent].x],
        #          [new_node.y, node_list[new_node.parent].y], color='blue', marker='o', markersize=0.5)
        # plt.show()
        # figure.canvas.draw()

    path2follow = np.array([np.array([g_pose.x]), np.array([g_pose.y])])
    path2follow = np.expand_dims(path2follow, 0)
    last_index = len(node_list) - 1

    while node_list[last_index].parent is not None:
        node = node_list[last_index]
        to_add = np.array([np.array([node.x]), np.array([node.y])])
        path2follow = np.concatenate([path2follow, [to_add]])
        last_index = node.parent

    return np.flipud(path2follow)


def pp(_, __, goal_pose, ____):
    return goal_pose
