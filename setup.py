#!/usr/bin/env python

from setuptools import setup, find_packages

# pp	1.66E-05	111.5307255	-0.001874849
# pp	1.82E-05	103.9729991	-0.001874849
# a_star	0.194547693	127.0403211	0.053945377
# a_star	0.194174302	126.8719471	0.053945377

setup(name='cormoran-simulator',
      version='1.0',
      description='Simulator of PINV15-177 ASV Cormoran for Python.',
      author='Federico Peralta',
      author_email='dev.peralta.federico@gmail.com',
      packages=find_packages(),
      install_requires=['scipy', 'numpy', 'matplotlib', 'pyyaml']
      )
